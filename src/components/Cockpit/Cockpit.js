import React, { useEffect, useRef, useContext } from 'react';
import classes from './Cockpit.css';
import AuthContext from '../../context/auth-context'

const cockpit = (props) => {
    const toggleBtnRef = useRef();
    const authContext = useContext(AuthContext);

    useEffect(() => {
        console.log("CockPit useEffect")
        // const timer=setTimeout(()=>{
        //     alert('saved Data to cloud');
        // },1000)
        toggleBtnRef.current.click();
        return () => {
            //clearTimeout(timer);
            console.log("CockPit.js UseEffectCleanUp")
        }
    }, [])

    useEffect(() => {
        console.log("CockPit 2nd useEffect")

        return () => { console.log("CockPit.js 2nd UseEffectCleanUp") }
    })
    const assignedClasses = [];
    let btnClass = '';
    if (props.showPersons) {
        btnClass = classes.Red;
    }

    if (props.personsLength <= 2) {
        assignedClasses.push(classes.red);
    }
    if (props.personsLength <= 1) {
        assignedClasses.push(classes.bold);
    }
    return (
        <div className={classes.Cockpit}>
            <h1>{props.title}</h1>
            <p className={assignedClasses.join(' ')}> I am really working</p>
            <button
                ref={toggleBtnRef}
                className={btnClass}
                onClick={props.clicked}>
                Toggle Persons!!
        </button>

            <button
                onClick={authContext.login}>
                Login
                    </button>
        </div>
    );
};

export default React.memo(cockpit);