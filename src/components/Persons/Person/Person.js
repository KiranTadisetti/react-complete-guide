import React, { Component } from 'react';
import classes from './Person.css';
import withClass from '../../../hoc/WithClass'
import Aux from '../../../hoc/Auxilary';
import PropTypes from 'prop-types'

import AuthContext from '../../../context/auth-context'

class Person extends Component {
    constructor() {
        super();
        this.inputElement = React.createRef();
    }

    static contextType = AuthContext;

    componentDidMount() {
        console.log(this.context.authenticated)
        this.inputElement.current.focus();
    }
    render() {
        console.log("Person.js rendering...")
        return (
            //<div className={classes.Person}>
            <Aux>

                <p>{this.context.authenticated ? 'Authenticated' : 'please login'}</p>

                <p onClick={this.props.click}> I'm {this.props.name} and {this.props.age} years old!</p>

                <p>{this.props.children}</p>
                <input
                    key='i3'
                    ref={this.inputElement}
                    type='text'
                    onChange={this.props.changed}
                    value={this.props.name} />

            </Aux>

        );
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func
}

export default withClass(Person, classes);