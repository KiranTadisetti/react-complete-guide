import React, { PureComponent } from 'react';
import Person from './Person/Person'

class Persons extends PureComponent {

    // static getDerivedStateFromProps(props,state){
    //     console.log("Persons.js getDerivedStateFromProps", props);
    //     return state;
    // }
    // shouldComponentUpdate(nextProps,nextState){
    //     console.log("Persons.js shouldComponentUpdate");
    //     if(nextProps.persons===this.props.persons){
    //         return false;
    //     }
    //     return true;
    // }
    getSnapshotBeforeUpdate(prevProps,prevState){
        console.log("Persons.js getSnapshotBeforeUpdate");
        return {message:'snapShot!'};
    }

    componentDidUpdate(prevProps,prevState,snapshot){
        console.log("Persons.js componentDidUpdate");
        console.log(snapshot)
    }

    componentWillUnmount(){
        console.log("persons.js componentWillUnMount")
    }

    render() {
        console.log("Persons.js rendering...")
        return this.props.persons.map((person, index) => {
            return <Person
                key={index}
                name={person.name}
                age={person.age}
                click={() => this.props.clicked(index)}
                changed={(event) => this.props.changed(event, person.id)}
                
            />
        })
    }
}

export default Persons;