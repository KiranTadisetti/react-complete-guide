import React, { Component } from 'react';
import classes from './App.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'
import Aux from '../hoc/Auxilary'
import withClass from '../hoc/WithClass';
import AuthContext from '../context/auth-context'

class App extends Component {
  constructor(props) {
    super(props);
    console.log('App.js Contructor');
  }


  state = {
    persons: [
      { id: 1, name: 'Kiran', age: 24 },
      { id: 2, name: 'Gokul', age: 21 },
      { id: 3, name: 'sindhu', age: 39 },

    ],
    otherState: "other",
    showPersons: false,
    showCockpit: true,
    changeCounter: 0,
    authunticated: false
  }

  static getDerivedStateFromProps(props, state) {
    console.log("App.js getDerivedStateFromProps", props);
    return state;
  }

  componentDidMount() {
    console.log("App.js componentDidMount")
  }
  shouldComponentUpdate(nextProps, nextState) {
    console.log("App.js shouldComponentUpdate")
    return true;
  }
  componentDidUpdate() {
    console.log("App.js componentDidUpdate")
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })

    const person = {
      ...this.state.persons[personIndex]
    }

    //update value
    person.name = event.target.value;
    //get the whole array
    const persons = [...this.state.persons]
    persons[personIndex] = person

    //setstate
    //this.setState({ persons: persons })
    this.setState((prevState, props) => {
      return {
        persons: persons,
        changeCounter: prevState.changeCounter + 1
      }
    })
  }


  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons]//this.state.persons.slice();
    persons.splice(personIndex, 1);
    this.setState({ persons: persons })
  }
  toggleCockpitHandler = () => {
    const doesShow = this.state.showCockpit;
    this.setState({ showCockpit: !doesShow });
  }
  loginHandler = () => {
    this.setState({ authunticated: true })
  }
  render() {
    console.log("App.js render");


    let persons = null;
    if (this.state.showPersons) {
      persons = <Persons
        persons={this.state.persons}
        clicked={this.deletePersonHandler}
        changed={this.nameChangeHandler}
        auth={this.state.authunticated}
      />
    }

    return (
      <Aux >
        <button onClick={this.toggleCockpitHandler}>Remove CockPit</button>
        <AuthContext.Provider value={{
          authenticated: this.state.authunticated,
          login: this.loginHandler
        }}>
          {this.state.showCockpit ? <Cockpit
            title={this.props.appTitle}
            showPersons={this.state.showPersons}
            personsLength={this.state.persons.length}
            clicked={this.togglePersonHandler}
            login={this.loginHandler}
          /> : null}

          {persons}
        </AuthContext.Provider>
      </Aux>
    );
  }
}

export default withClass(App, classes.App);
